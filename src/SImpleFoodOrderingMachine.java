import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SImpleFoodOrderingMachine {
    private JPanel root;
    private JButton tempuraButton;
    private JButton ramenButton;
    private JButton udonButton;
    private JTextPane textPane1;

    void order(String food){
        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "Would you like to order "+ food+"?",
                "order Confirmation",
                JOptionPane.YES_NO_OPTION);
        if(confirmation==0){
            textPane1.setText("Thank you for order "+food);
            JOptionPane.showMessageDialog(null, "Order for "+food+" reserved.");
        }
    }


    public SImpleFoodOrderingMachine() {
        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tempura");

            }
        });
        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ramen");
            }

        });
        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Udon");
            }

        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("SImpleFoodOrderingMachine");
        frame.setContentPane(new SImpleFoodOrderingMachine().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
